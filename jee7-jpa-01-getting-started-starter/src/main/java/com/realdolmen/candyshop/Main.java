package com.realdolmen.candyshop;

import com.realdolmen.candyshop.domain.Person;
import com.realdolmen.candyshop.repository.PersonRepository;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        LocalDate date = LocalDate.of(1988, Month.DECEMBER, 11);
        System.out.println(date.hashCode());
        date = date.plusDays(10);
        System.out.println(date.hashCode());

        LocalDate d = LocalDate.now();
        LocalDateTime e = d.atTime(13, 37);
        LocalDateTime f = d.atStartOfDay();
        System.out.println(f);

        LocalDate now = LocalDate.now();
        LocalDate then = LocalDate.of(1987, Month.JANUARY, 8);
        Period p = Period.between(then, now);
        int m = p.getMonths();
        int y = p.getYears();
        long c = p.get(ChronoUnit.DAYS);
        System.out.println(m+" "+y+" "+c);
    }
}
