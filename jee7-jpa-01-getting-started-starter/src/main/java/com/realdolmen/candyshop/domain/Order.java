
package com.realdolmen.candyshop.domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "candy_order")
public class Order {
    
    @Id
    @GeneratedValue
    private Long id;
    
    @Embedded
    private Address deliveryAddress;
    
    @ManyToOne
    private Person person;
    
    @OneToMany
    @JoinColumn(name = "order_id")
    private List<OrderLine> orderLines;
    
    public Order() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long Id) {
        this.id = id;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }
    
    public Double calculateTotal(){
        if(!(this.orderLines.isEmpty())){
        double totalCost = 0;
        List<OrderLine> oList = this.orderLines; 
        for(OrderLine o : oList){
            totalCost += o.calculateSubTotal();
        }
        return totalCost;
        } 
        return 0.00;
}
}
