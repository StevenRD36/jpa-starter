package com.realdolmen.candyshop.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;


// TODO: Make person a JPA entity
@Entity
@NamedQueries({
        @NamedQuery(name = Person.FIND_COUNT, query = "select count(p.id) from Person p"),
        @NamedQuery(name = Person.FIND_LIKE_FIRST, query = "select p from Person p where p.firstName like :firstname"),
        @NamedQuery(name = Person.FIND_LIKE_LAST, query = "select p from Person p where p.lastName like :lastname")
})
public class Person extends AbstractEntity {
    // TODO: add property 'id' (Long) which will be the auto-generated primary key
    public static final String FIND_COUNT = "findCount";
    public static final String FIND_LIKE_FIRST = "findLikeFirstName";
    public static final String FIND_LIKE_LAST = "findLikeLastName";


    @Id
    @GeneratedValue
    private Long id;
    // TODO: add properties 'firstName' and 'lastName' (String)
    @Column(nullable = false, length = 200)
    private String firstName, lastName;

    @Lob
    private Byte[] image;

    @Column(nullable = false)
    private LocalDate birthDate;

    @Transient
    private int age;

    @ElementCollection
    @CollectionTable(name = "candy_preferences")
    @Enumerated(EnumType.STRING)
    @Column(name = "candy_color")
    private List<Color> candyPreferences;

    @Embedded
    private Address address;

    public Person() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public int getAge() {
        return age;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Color> getCandyPreferences() {
        return candyPreferences;
    }

    public void setCandyPreferences(List<Color> candyPreferences) {
        this.candyPreferences = candyPreferences;
    }

    public Byte[] getImage() {
        return image;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return firstName +" "+lastName ;
    }
}
