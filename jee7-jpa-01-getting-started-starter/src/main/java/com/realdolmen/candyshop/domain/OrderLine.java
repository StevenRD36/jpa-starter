
package com.realdolmen.candyshop.domain;
import javax.persistence.*;

@Entity
@Table(name = "order_line")
public class OrderLine {
    
    @Id
    @GeneratedValue
    private Long id;
    
    private Integer quantity;
    
    @ManyToOne
    private Candy candy;

    public OrderLine() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Candy getCandy() {
        return candy;
    }

    public void setCandy(Candy candy) {
        this.candy = candy;
    }
    
    public Float calculateSubTotal(){
        if(this.candy!=null){
            return quantity*this.candy.getPrice();
        }
        return 0.00f;
    }
    
    
    
}
