package com.realdolmen.candyshop.mappers;

import com.realdolmen.candyshop.domain.Address;
import com.realdolmen.candyshop.domain.Person;
import com.realdolmen.candyshop.dtos.AddressDTO;
import com.realdolmen.candyshop.dtos.PersonDTO;

import java.util.function.Function;

public class PersonMapper implements Function<Person,PersonDTO> {

    @Override
    public PersonDTO apply(Person person) {
        PersonDTO dto = new PersonDTO(person.getId(),person.getFirstName(),person.getLastName(),person.getBirthDate(),person.getAge());
        if(person.getAddress()!=null){
            Address address = person.getAddress();
            dto.setAddress(new AddressDTO(address.getStreet(),address.getNumber(),address.getPostalCode(),address.getCity()));
        }
        return dto;
    }
}
