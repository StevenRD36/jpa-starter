package com.realdolmen.candyshop.repository;

import com.realdolmen.candyshop.domain.Candy;

import javax.persistence.EntityManager;

public class CandyRepository extends AbstractRepository<Candy,Long> {

    public CandyRepository(EntityManager em) {
        super(em, Candy.class);
    }
}
