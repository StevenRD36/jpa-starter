package com.realdolmen.candyshop.repository;

import com.realdolmen.candyshop.domain.Address_;
import com.realdolmen.candyshop.domain.Person;
import com.realdolmen.candyshop.domain.Person_;
import com.realdolmen.candyshop.dtos.SearchParams;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class PersonRepository extends AbstractRepository<Person, Long> {

    public PersonRepository(EntityManager em) {
        super(em, Person.class);
    }

    public Long countPerson() {
        return em.createNamedQuery(Person.FIND_COUNT, Long.class).getSingleResult();
    }

    public List<Person> findPersonsByFirstName(String firstname) {
        return em.createNamedQuery(Person.FIND_LIKE_FIRST, Person.class)
                .setParameter("firstname", firstname)
                .getResultList();
    }

    public List<Person> findPersonsByLastName(String lastName) {
        return em.createNamedQuery(Person.FIND_LIKE_LAST, Person.class)
                .setParameter("lastname", lastName)
                .getResultList();
    }

    public List<Person> searchFunction(SearchParams searchParams) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Person> criteriaQuery = builder.createQuery(Person.class);
        Root<Person> root = criteriaQuery.from(Person.class);
        List<Predicate> predicates = new ArrayList<>();
        if (searchParams.getFirstName() != null) {
            predicates.add(builder.like(root.get(Person_.firstName), searchParams.getFirstName()));
        }
        if (searchParams.getLastname() != null) {
            predicates.add(builder.like(root.get(Person_.lastName), searchParams.getLastname()));
        }
        if(searchParams.getCity() != null){
            predicates.add(builder.like(root.get(Person_.address).get(Address_.city),searchParams.getCity()));
        }
        if(searchParams.getStreet() != null){
            predicates.add(builder.like(root.get(Person_.address).get(Address_.street),searchParams.getStreet()));
        }
        if(searchParams.getPostalcode() != null){
            predicates.add(builder.like(root.get(Person_.address).get(Address_.postalCode),searchParams.getPostalcode()));
        }

        criteriaQuery.select(root).where(builder.and(predicates.toArray(new Predicate[]{})));

        Query q = em.createQuery(criteriaQuery);
        return q.getResultList();
    }
}
