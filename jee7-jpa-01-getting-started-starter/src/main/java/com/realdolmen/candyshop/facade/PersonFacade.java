package com.realdolmen.candyshop.facade;

import com.realdolmen.candyshop.domain.Person;
import com.realdolmen.candyshop.dtos.PersonDTO;
import com.realdolmen.candyshop.mappers.PersonMapper;
import com.realdolmen.candyshop.services.PersonService;

import java.util.List;
import java.util.stream.Collectors;

public class PersonFacade {

    private PersonService personService;

    public PersonFacade(PersonService personService) {
        this.personService = personService;
    }

    public List<PersonDTO> findAllPersons() {
        List<Person> people = personService.findAllPersons();
        return people.stream()
                .map(person -> new PersonMapper().apply(person))
                .sorted((p1, p2) -> p1.getLastName().compareTo(p2.getLastName()))
                .collect(Collectors.toList());
    }
}
