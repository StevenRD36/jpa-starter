package com.realdolmen.candyshop.services;

import com.realdolmen.candyshop.domain.Person;
import com.realdolmen.candyshop.dtos.SearchParams;
import com.realdolmen.candyshop.repository.PersonRepository;

import java.util.List;
import java.util.stream.Collectors;

public class PersonService {

    private PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> findAllPersons() {
        return personRepository.findAll();
    }

    public List<Person> findPersonByFirstName(String firstName) {
        return personRepository.findPersonsByFirstName(firstName);
    }

    public List<Person> findPersonByLastName(String lastName) {
        return personRepository.findPersonsByLastName(lastName);
    }

    public void addPerson(Person person) {
        personRepository.save(person);
    }

    public List<Person> searchFunction(SearchParams searchParams){
        List<Person> persons = personRepository.searchFunction(searchParams);
        //not possible to find the object birtdate in the repository
        if(searchParams.getYear()!=null){
            persons = persons.stream().filter(person -> person.getBirthDate().getYear() == searchParams.getYear().getValue())
                    .collect(Collectors.toList());
        }
        if(searchParams.getMonth()!=null){
            persons = persons.stream().filter(person -> person.getBirthDate().getMonth() == searchParams.getMonth())
                    .collect(Collectors.toList());
        }
        return persons;
    }
}
