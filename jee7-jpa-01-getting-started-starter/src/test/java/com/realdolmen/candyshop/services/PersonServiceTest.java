package com.realdolmen.candyshop.services;

import com.realdolmen.candyshop.domain.Person;
import com.realdolmen.candyshop.dtos.SearchParams;
import com.realdolmen.candyshop.repository.PersonRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PersonServiceTest {

    @Mock
    private PersonRepository personRepository;

    private PersonService personService;
    private List<Person> persons;

    @Before
    public void init() {
        personService = new PersonService(personRepository);
        persons = new ArrayList<>();
        Person person = new Person();
        person.setBirthDate(LocalDate.of(1980, Month.DECEMBER, 5));
        Person person2 = new Person();
        person2.setBirthDate(LocalDate.of(1981, Month.JANUARY, 5));
        persons.add(person);
        persons.add(person2);
    }

    @Test
    public void findAllPersonsTest() {

        when(personRepository.findAll()).thenReturn(persons);

        List<Person> result = personService.findAllPersons();

        assertEquals(result.size(), persons.size());
        verify(personRepository, times(1)).findAll();
    }

    @Test
    public void searchPersonYearTest() {

        SearchParams searchParams = new SearchParams();
        searchParams.setYear(Year.of(1980));
        when(personRepository.searchFunction(searchParams)).thenReturn(persons);

        List<Person> result = personService.searchFunction(searchParams);

        assertEquals(result.size(), persons.size() - 1);
        verify(personRepository, times(1)).searchFunction(searchParams);
    }

    @Test
    public void searchPersonMonthTest() {

        SearchParams searchParams = new SearchParams();
        searchParams.setMonth(Month.DECEMBER);
        when(personRepository.searchFunction(searchParams)).thenReturn(persons);

        List<Person> result = personService.searchFunction(searchParams);

        assertEquals(result.size(), persons.size() - 1);
        verify(personRepository, times(1)).searchFunction(searchParams);
    }

    @Test
    public void searchPersonMonthAndYearTest() {

        SearchParams searchParams = new SearchParams();
        searchParams.setMonth(Month.DECEMBER);
        searchParams.setYear(Year.of(1980));
        when(personRepository.searchFunction(searchParams)).thenReturn(persons);

        List<Person> result = personService.searchFunction(searchParams);

        assertEquals(result.size(), persons.size() - 1);
        verify(personRepository, times(1)).searchFunction(searchParams);
    }

    //TODO tests afwerken

}
