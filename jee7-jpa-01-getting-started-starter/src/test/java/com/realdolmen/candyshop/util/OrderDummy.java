package com.realdolmen.candyshop.util;

import com.realdolmen.candyshop.domain.*;

import java.util.ArrayList;
import java.util.List;

public class OrderDummy {

    public static List<Order> getOrderList(int amount){

        List<Order> orders = new ArrayList<>();
        for(int i = 0; i<amount;i++){
            orders.add(getOrder(i+1));
        }
        return orders;
    }

    public static Order getOrder(int amount){
        Order order = new Order();
        List<OrderLine> orderLines = new ArrayList<>();
        for(int i = 0 ; i< amount ; i++){
            OrderLine orderLine = new OrderLine();
            orderLine.setQuantity(2*i+2);
            Candy candy = new GummyBears();
            candy.setPrice(new Float(5*i+3));
            orderLine.setCandy(candy);
            orderLines.add(orderLine);
        }
        order.setOrderLines(orderLines);
        return order;
    }
}
