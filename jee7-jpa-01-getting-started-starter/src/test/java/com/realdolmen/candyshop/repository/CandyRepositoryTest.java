package com.realdolmen.candyshop.repository;

import com.realdolmen.candyshop.domain.Address;
import com.realdolmen.candyshop.domain.ChocolateBar;
import com.realdolmen.candyshop.domain.Color;
import com.realdolmen.candyshop.domain.Candy;
import com.realdolmen.candyshop.util.DateUtils;
import org.junit.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Temporal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class CandyRepositoryTest {
    private static EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    private static CandyRepository candyRepository;

    @BeforeClass
    public static void initClass() {
        entityManagerFactory = Persistence.createEntityManagerFactory("CandyShopPersistenceUnit");
    }

    @Before
    public void init() {
        entityManager = entityManagerFactory.createEntityManager();
        candyRepository = new CandyRepository(entityManager);
    }

    @Test
    public void findCandyByIdTest() {
        Candy Candy = candyRepository.findById(4000L);
        assertNotNull(Candy);
    }

    @Test
    public void saveCandyChoco() throws Exception {
        ChocolateBar chocolateBar = new ChocolateBar();
        chocolateBar.setName("Milky way");
        chocolateBar.setPrice(5.55f);
        chocolateBar.setLength(10);
        candyRepository.save(chocolateBar);

        //transaction is closed

        Candy pp = entityManager.find(Candy.class, chocolateBar.getId());
        assertEquals("Milky way",pp.getName());

    }

    @Test
    public void deleteCandyTest(){
        candyRepository.delete(1000L);
        //transaction is closed
        assertEquals(null, entityManager.find(Candy.class,1000L));
    }

    @Test
    public void updateCandyTest(){
        Candy p = entityManager.find(Candy.class,3000L);
        candyRepository.begin();
        entityManager.detach(p);
        p.setName("Bob");
        entityManager.flush();
        entityManager.clear();
        candyRepository.update(p);

        //transaction is closed
        assertEquals("Bob", entityManager.find(Candy.class, 3000L).getName());
    }

    @Test
    public void findAllCandyTest(){
        List<Candy> candies = candyRepository.findAll();
        assertFalse(candies.isEmpty());
    }


    @After
    public void exit() {
        candyRepository.close();
    }

    @AfterClass
    public static void exitClass(){
        entityManagerFactory.close();
    }
}
