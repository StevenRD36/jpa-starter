package com.realdolmen.candyshop.repository;

import com.realdolmen.candyshop.domain.Address;
import com.realdolmen.candyshop.domain.Color;
import com.realdolmen.candyshop.domain.Person;
import com.realdolmen.candyshop.dtos.SearchParams;
import org.junit.*;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class PersonRepositoryTest {

    private static EntityManagerFactory emf;
    private EntityManager em;
    private PersonRepository personRepository;
    private SearchParams searchParams;

    @BeforeClass
    public static void initClass() {
        emf = Persistence.createEntityManagerFactory("CandyShopPersistenceUnit");
    }


    @Before
    public void init() {
        System.out.println("init personRepository");
        em = emf.createEntityManager();
        personRepository = new PersonRepository(em);
        searchParams = new SearchParams();

    }

    @Test
    public void findPersonByIdTest() {
        Person person = personRepository.findById(4000L);
        assertNotNull(person);
    }

    public Byte[] getImageData() {
        File imgPath = new File("Path\\to\\image");
        BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageIO.read(imgPath);
            // get DataBufferBytes from Raster
            WritableRaster raster = bufferedImage.getRaster();
            DataBufferByte data = (DataBufferByte) raster.getDataBuffer();
            byte[] dataa = data.getData();
            Byte[] bytes = new Byte[dataa.length];
            int i = 0;
            for (byte b : dataa) {
                bytes[i++] = b;
            }
            return bytes;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Test
    public void savePerson() throws Exception {
        Person p = new Person();
        p.setBirthDate(LocalDate.of(1988, Month.AUGUST, 5));
        p.setFirstName("Amy");
        p.setLastName("Winehouse");
        Address address = new Address();
        address.setStreet("Vaucampslaan");
        address.setNumber("42");
        address.setCity("Huizingen");
        address.setPostalCode("1645");
        p.setAddress(address);
        p.setCandyPreferences(new ArrayList<>(Arrays.asList(Color.BLUE, Color.PINK)));
        personRepository.save(p);

        //transaction is closed

        Person pp = em.find(Person.class, p.getId());
        assertEquals(LocalDate.of(1988, Month.AUGUST, 5), pp.getBirthDate());
        assertEquals("Amy", pp.getFirstName());
        assertEquals("Winehouse", pp.getLastName());
        assertEquals("Vaucampslaan", pp.getAddress().getStreet());
        assertEquals("42", pp.getAddress().getNumber());
        assertEquals("Huizingen", pp.getAddress().getCity());
        assertEquals(Color.BLUE, pp.getCandyPreferences().get(0));
        assertEquals(Color.PINK, pp.getCandyPreferences().get(1));

    }

    @Test
    public void findAllTest() {
        List<Person> persons = personRepository.findAll();
        assertFalse(persons.isEmpty());
    }

    @Test
    public void deletePersonTest() {
        personRepository.delete(1000L);
        //transaction is closed
        assertEquals(null, em.find(Person.class, 1000L));
    }

    @Test
    public void updatePersonTest() {
        Person p = em.find(Person.class, 3000L);
        personRepository.begin();
        em.detach(p);
        p.setFirstName("Bob");
        em.flush();
        em.clear();
        personRepository.update(p);

        //transaction is closed
        assertEquals("Bob", em.find(Person.class, 3000L).getFirstName());
    }


    @Test
    public void countPersonTest() {
        Long result = personRepository.countPerson();
        assertTrue(result > 0);
    }

    @Test
    public void findPersonsByFirstNameTest() {
        List<Person> persons = personRepository.findPersonsByFirstName("mar%");
        assertFalse(persons.isEmpty());
    }

    @Test
    public void searchFunctionTest() {
        searchParams.setFirstName("Jane");
        searchParams.setLastname("Doe");
        List<Person> persons = personRepository.searchFunction(searchParams);
        assertNotNull(persons);
        assertFalse(persons.isEmpty());
    }


    @Test
    public void searchPersonAddressTest() {
        searchParams.setCity("London");
        List<Person> persons = personRepository.searchFunction(searchParams);
        assertNotNull(persons);
        assertFalse(persons.isEmpty());
    }

    @After
    public void exit() {
        personRepository.close();
    }

    @AfterClass
    public static void exitClass() {
        emf.close();
    }

}
